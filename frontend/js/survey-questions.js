import SurveyQuestion from './survey-question.js'
import SurveyCompleted from './survey-completed.js'
import SurveyHeader from './survey-header.js'
import SurveyControls from './survey-controls.js'

const template = document.createElement('template')

template.innerHTML = `
    <style>
     :host([hidden]) { display: none }
     :host {
	 --color-valid: green;
     }
     :host {
	 display: flex;
	 flex-direction: column;
	 justify-content: center;
	 border-bottom: 1rem solid var(--color-valid);
	 min-height: 100vh;
	 width: 100%;
     }
     .Component {
	 display: flex;
	 flex-direction: column;
	 align-items: center;
     }
    </style>
    <div class="Component"></div>
`

const SurveyQuestions = class extends HTMLElement {
    static get observedAttributes() {
	return ['survey-id']
    }
    constructor() {
	super()
	this.attachShadow({mode: 'open'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    async attributeChangedCallback () {
	const data = await this.fetchSurveyData()
	this.setSurveyData(data)
	this.render()
    }
    async connectedCallback () {
	const data = await this.fetchSurveyData()
	this.setSurveyData(data)
	this.attachEvents()
	this.render()
    }
    disconnectedCallback = () => {
	this.removeEvents()
    }
    setSurveyData = (data) => {
	if(!data) return
	this.completed = false
	this.title = data.title
	this.description = data.description
	this.questions = data.questions
    }
    fetchSurveyData = async () => {
	let apiRootUrl
	if (window.location.hostname === 'localhost') {
	    apiRootUrl = 'http://localhost:5000/survey'
	} else {
	    apiRootUrl = this.getAttribute('api-url') || ''
	}

	this.surveyId = this.getAttribute('survey-id') || ''
	if (!this.surveyId) {
	    console.log('The survey-id parameter is required')
	    return
	}
	this.apiUrl = `${apiRootUrl}/${this.surveyId}`

	let data = {}
	try {
	    data = await fetch(this.apiUrl).then(res => res.json())
	} catch (error) {
	    console.log('Error fetching ressource', error)
	}
	return data
    }
    attachEvents = () => {
	this.shadowRoot
	    .addEventListener('submitSurvey', this.handleSubmit, false)
	this.shadowRoot
	    .addEventListener('nextQuestion', this.handleNext, false)
	this.shadowRoot
	    .addEventListener('newSurvey', this.handleNewSurvey, false)
    }
    removeEvents = () => {
	this.shadowRoot
	    .removeEventListener('submitSurvey', this.handleSubmit)
	this.shadowRoot
	    .removeEventListener('nextQuestion', this.handleNext)
	this.shadowRoot
	    .removeEventListener('newSurvey', this.handleNewSurvey)
    }
    handleNewSurvey = () => {
	location.reload()
    }
    getAnswers = () => {
	let $questions = this.shadowRoot.querySelectorAll('survey-question')
	$questions = Array.prototype.slice.call($questions)

	let answers = $questions.map(question => {
	    return {
		question: question.id,
		answer: question.answer
	    }
	}).filter(item => item.answer)

	return answers
    }
    handleSubmit = async () => {
	const answers = this.getAnswers()
	const errors = await this.checkForErrors(answers)

	if (errors.length) {
	    return
	}

	let res
	try {
	    res = await this.postSurveyAnswers(answers)
	    res = await res.json()
	} catch(error) {
	    console.log('error posting')
	    return
	}

	this.completed = true
	this.completedMessage = res.message
	this.render()
    }
    checkForErrors = async (answers) => {
	const questions = this.questions

	const errors = this.validateAnswers(answers, questions)

	if (errors.length) {
	    this.handleErrors(errors, answers, questions)
	    return errors
	}
	return []
    }
    handleNext = () => {
	const answers = this.getAnswers()
	this.checkForErrors(answers)
    }
    validateAnswers = (answers, questions) => {
	let answeredQuestions = answers.map(answer => answer.question)
	let missingQuestions = []
	questions.forEach(question => {
	    let didAnswerQuestion = answeredQuestions.indexOf(question.id.toString()) >= 0
	    if (!didAnswerQuestion) {
		missingQuestions.push(question)
	    }
	})
	return missingQuestions
    }
    handleErrors = (errors, answers, questions) => {
	const firstEl = this.shadowRoot.querySelector(`survey-question[id="${errors[0].id}"]`)
	firstEl.scrollIntoView({
	    behavior: 'smooth'
	})
    }

    postSurveyAnswers = async (answers) => {
	const data = {
	    message: 'Posting survey answers from survey-questions web-component',
	    surveyId: this.surveyId,
	    answers
	}
	
	return await fetch(this.apiUrl, {
	    method: 'POST',
	    mode: 'cors',
	    cache: 'no-cache',
	    credentials: 'same-origin',
	    headers: {
		'Content-Type': 'application/json'
	    },
	    redirect: 'follow',
	    referrerPolicy: 'no-referrer',
	    body: JSON.stringify(data)
	})
    }
    
    render() {
	let $component = this.shadowRoot.querySelector('.Component')
	$component.innerHTML = ''

	if (!this.questions) {
	    let message = document.createElement('p')
	    message.innerHTML = 'This survey has at the moment no question.'
	    $component.appendChild(message)
	    return
	}


	let header = document.createElement('survey-header')
	header.setAttribute('title', this.title)
	header.setAttribute('description', this.description)
	header.setAttribute('total', this.questions.length)

	if (this.completed) {
	    let surveyCompleted = document.createElement('survey-completed')
	    surveyCompleted.setAttribute('title', 'Completed')
	    surveyCompleted.setAttribute('message', this.completedMessage)
	    $component.appendChild(header)
	    $component.appendChild(surveyCompleted)
	    return 
	}


	$component.appendChild(header)
	this.questions.forEach((item, index) => {
	    let question = document.createElement('survey-question')
	    question.setAttribute('id', item.id)
	    question.setAttribute('title', item.Headline)
	    question.setAttribute('index', index + 1)
	    question.setAttribute('total', this.questions.length)
	    $component.appendChild(question)
	})
	
	let controls = document.createElement('survey-controls')
	$component.appendChild(controls)	    
    }
}

customElements.define('survey-questions', SurveyQuestions)

export default SurveyQuestions
