import QuestionAnswer from './question-answer.js'

const template = document.createElement('template')

template.innerHTML = `
    <style>
     :host([hidden]) { display: none }
     :host {
	 box-sizing: border-box;
	 display: flex;
	 justify-content: center;
	 align-items: center;
	 min-height: 90vh;
	 max-width: 40rem;
	 width: 100%;
	 padding: 1rem;

	 position: relative;
     }
     .Index {
	 position: absolute;
	 top: 20vh;
	 left: 5vw;
     }
     .Title {
	 font-style: italic;
	 font-size: 1.4rem;
     }
     .Component {
	 width: 100%;
     }
    </style>
    <div class="Component"></div>
`

const SurveyQuestion = class extends HTMLElement {
    constructor() {
	super()
	this.attachShadow({mode: 'open'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    async connectedCallback() {
	this.id = this.getAttribute('id')
	this.title = this.getAttribute('title')
	this.index = this.getAttribute('index')
	this.total = this.getAttribute('total')
	this.attachEvents()
	this.render()
    }
    attachEvents = () => {
	this.shadowRoot
	    .addEventListener('submitAnswer', this.handleSubmit, false)
    }
    handleSubmit = (event) => {
	this.answer = event.detail
    }
    render() {
	let $component = this.shadowRoot.querySelector('.Component')

	let index = document.createElement('span')
	index.classList.add('Index')
	index.innerHTML = `${this.index} / ${this.total}`
	$component.appendChild(index)

	let question = document.createElement('p')
	question.innerHTML = this.title
	question.classList.add('Title')
	$component.appendChild(question)

	let answer = document.createElement('question-answer')
	answer.innerHTML = this.title
	answer.classList.add('Title')
	$component.appendChild(answer)
    }
}

customElements.define('survey-question', SurveyQuestion)

export default SurveyQuestion
