const template = document.createElement('template')

template.innerHTML = `
    <style>
     :host([hidden]) { display: none }
     :host {
	 display: flex;
	 justify-content: flex-end;
	 margin-top: 3rem;
     }
     select {
	 min-width: 10rem;
	 background-color: transparent;
	 padding: 1rem 2rem;
	 cursor: pointer;
     }
    </style>
    <div class="Component"></div>
`

const QuestionAnswer = class extends HTMLElement {
    constructor() {
	super()
	this.attachShadow({mode: 'open'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    async connectedCallback() {
	this.render()
    }
    handleChange = (event) => {
	const answer = this.getAnswer()
	this.submitAnswer(answer)
    }
    getAnswer = () => {
	var $select = this.shadowRoot.querySelector('select')
	var value = $select.options[$select.selectedIndex].value;
	return value
    }
    submitAnswer = (answer) => {
	const event = new CustomEvent('submitAnswer', {
	    bubbles: true,
	    detail: answer
	})
	this.dispatchEvent(event)
    }
    render() {
	let $component = this.shadowRoot.querySelector('.Component')
	let values = [1,2,3,4,5,6]

	let select = document.createElement('select')
	select.setAttribute('title', 'Your decision')

	let defaultOption = document.createElement('option')
	defaultOption.setAttribute('disabled', true)
	defaultOption.setAttribute('selected', true)
	defaultOption.setAttribute('value', '')
	defaultOption.innerHTML = '—'
	select.appendChild(defaultOption)
	
	values.forEach(value => {
	    let option = document.createElement('option')
	    option.setAttribute('value', value)
	    option.innerHTML = value
	    select.appendChild(option)
	})
	select.onchange = this.handleChange
	
	$component.appendChild(select)
    }
}

customElements.define('question-answer', QuestionAnswer)

export default QuestionAnswer
