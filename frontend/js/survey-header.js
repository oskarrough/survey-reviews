const template = document.createElement('template')

template.innerHTML = `
    <style>
     :host([hidden]) { display: none }
     :host {
	 --color-bg-button: lightgray;
	 --color-bg-intro: gold;
     }
     :host {
	 display: flex;
	 flex-direction: column;
	 justify-content: center;
	 align-items: center;
	 min-height: 100vh;
	 width: 100%;
	 
	 text-align: center;
	 background-color: var(--color-bg-intro);
	 font-size: 1.4rem;
     }
    </style>
    <div class="Component"></div>
`

const SurveyHeader = class extends HTMLElement {
    constructor() {
	super()
	this.attachShadow({mode: 'open'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    async connectedCallback() {
	this.title = this.getAttribute('title')
	this.description = this.getAttribute('description')
	this.total = this.getAttribute('total')
	this.render()
    }
    render() {
	let $component = this.shadowRoot.querySelector('.Component')
	
	let title = document.createElement('h1')
	title.innerHTML = this.title || 'Survey'
	$component.appendChild(title)

	let description = document.createElement('p')
	description.innerHTML = this.description || 'Answer the following questions'
	$component.appendChild(description)

	let total = document.createElement('small')
	total.innerHTML = `<i>${this.total} questions</i>`
	$component.appendChild(total)
    }
}

customElements.define('survey-header', SurveyHeader)

export default SurveyHeader
