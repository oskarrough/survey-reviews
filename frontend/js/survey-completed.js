const template = document.createElement('template')

template.innerHTML = `
    <style>
     :host([hidden]) { display: none }
     :host {
	 --color-bg-button: lightgray;
     }
     :host {
	 position: fixed;
	 bottom: 3rem;
	 right: 3rem;
	 z-index: 1;
	 display: flex;
	 align-items: center;
	 justify-content: center;
	 background-color: white;
	 padding: 2rem;
     }
     .Component {
	 display: flex;
	 flex-direction: column;
	 align-items: center;
	 justify-content: center;
     }
     button {
	 background-color: var(--color-button-bg);
	 padding: 0.5rem;
	 font-size: 1.2rem;
	 cursor: pointer;
	 border-width: 0.3rem;
	 border-color: lightgray;
	 margin-left: 1rem;
     }
    </style>
    <div class="Component"></div>
`

const SurveyCompleted = class extends HTMLElement {
    constructor() {
	super()
	this.attachShadow({mode: 'open'})
	this.shadowRoot.appendChild(template.content.cloneNode(true))
    }
    async connectedCallback() {
	this.title = this.getAttribute('title')
	this.message = this.getAttribute('message')
	this.render()
    }
    handleNewSurvey = () => {
	const event = new CustomEvent('newSurvey', {
	    bubbles: true
	})
	this.dispatchEvent(event)
    }
    render() {
	let $component = this.shadowRoot.querySelector('.Component')
	
	let message = document.createElement('p')
	message.innerHTML = this.message || 'Thank you for completing this survey!'
	$component.appendChild(message)

	let button = document.createElement('button')
	button.innerHTML = 'Take the survey again'
	button.onclick = this.handleNewSurvey
	$component.appendChild(button)
    }
}

customElements.define('survey-completed', SurveyCompleted)

export default SurveyCompleted
